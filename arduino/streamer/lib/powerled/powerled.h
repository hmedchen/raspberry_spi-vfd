#define POWER_ON_BRIGHT 255
#define POWER_OFF_BRIGHT 100

#define POWER_OFF 0
#define POWER_ON 1
#define STARTUP 2
#define SHUTDOWN 3

class PowerLED
{
public:
    // Vars
    int LED = 9;
    int brightness[10] = {0,10,20,30,40,50,60,70,90,100};
    int bright[100];
    int maxval = 255;
    int speedup = 2;
    int speeddn = 2;

    // Methods
    PowerLED();
    void cycleup_sin(int speed = 2);
    void cycledn_sin(int speed = 2);
    void startup();
    void shutdown();
    void pulse();
    void show_state(int state);
};
