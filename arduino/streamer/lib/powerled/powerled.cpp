#include "powerled.h"
#include <Arduino.h>

PowerLED::PowerLED()
{
  pinMode(LED, OUTPUT);
  int i;
  float upper;
  // init fade array
  // TODO: two arrays, one for linear one for sin
  upper = 3.14 / 2;
  for (i=0; i<100; i++)
  {
     bright[i] = sin(upper / 100 * i) * maxval;
  }
}

void PowerLED::cycleup_sin(int speedup)
{
  int b;
  for (b = 0; b < int(100/speedup); b++)
  {
    analogWrite(LED, bright[b* speedup]);
//      Serial.println(bright[b]);
    delay(15);
  }
}

void PowerLED::cycledn_sin(int speeddn)
{
  int b;
  for (b = int(99/speeddn); b >= 0; b--)
  {
    analogWrite(LED, bright[b* speeddn]);
    delay(15);
  }
}

void PowerLED::startup()
{
    this->cycleup_sin(1);
    this->cycledn_sin(15);
    delay(100);
}

void PowerLED::shutdown()
{
    this->cycleup_sin(15);
    this->cycledn_sin(1);
    delay(100);
}

void PowerLED::pulse()
{
    this->cycleup_sin(1);
    delay(500);
    this->cycledn_sin(1);
    delay(500);
}

void PowerLED::show_state(int rpi_state = 0)
{
    switch (rpi_state) {
      case POWER_OFF:
          analogWrite(LED, POWER_OFF_BRIGHT);
          break;
      case POWER_ON:
          analogWrite(LED, POWER_ON_BRIGHT);
          break;
      case STARTUP:
          startup();
          break;
      case SHUTDOWN:
          shutdown();
          break;
      default:
          pulse();
          break;
    }
}
