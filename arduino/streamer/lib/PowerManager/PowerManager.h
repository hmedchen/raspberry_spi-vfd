#include <powerled.h>

#define POWER_OFF 0
#define POWER_ON 1
#define STARTUP 2
#define SHUTDOWN 3


class PowerManager {
    public:
        unsigned int pi_shutdown_detected;
        bool power = true;
        int myval = 0;
        int shutoff_pin;
        int rpi_state;
        PowerLED powerled;

        PowerManager();
        void power_off_pi();
        void power_on_pi();
        void listen_pwr_button();
        void check_for_poweroff();
        void loop();
};
