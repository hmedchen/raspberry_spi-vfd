#include <powerled.h>

#define RPI_ASSUMED_OFF 0
#define RPI_ASSUMED_STARTING 1
#define RPI_ASSUMED_ON 2
#define RPI_ASSUMED_SHUTTING_DOWN 3

//inputs
#define PWRBUTTON 2
#define RPI_STATUS_BOOTED 6
#define RPI_STATUS_OFF 7

//outputs
// power relays for rac and Raspberry Pi power supply
#define RELAY_DAC 9
#define RELAY_RPI 10
// raspberry pi shutdown via GPIO
#define RPI_SEND_SHUTDOWN 5

// constants
// grace period from first shutdown pin high to shutdown
#define SHUTDOWN_GRACE 500
// difference in shutdown between RELAY_DAC and RELAY_PI
#define SHUTDOWN_DIFF 100

class PowerManager {
    public:
        unsigned int pi_shutdown_detected;
        bool power = true;
        int myval = 0;
        int shutoff_pin;
        int rpi_state;
        PowerLED powerled;

        PowerManager();
        void power_off_pi();
        void power_on_pi();
        void check_status();
//        void listen_pwr_button();
//        void check_for_poweroff();
        void loop();
};
