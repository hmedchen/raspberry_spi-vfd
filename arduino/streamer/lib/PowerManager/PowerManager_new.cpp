#include <Arduino.h>
#include "PowerManager.h"
//#include <powerled.h>

//inputs
//byte PWRBUTTON = 2;
//byte RPI_BOOTED = 6;
//byte RPI_OFF = 7;

//outputs
// power relays for rac and Raspberry Pi power supply
//byte RELAY_DAC = 7;
//byte RELAY_RPI = 8;
// raspberry pi shutdown via GPIO
//byte RPI_SHUTDOWN = 5;

// constants
// grace period from first shutdown pin high to shutdown
//int SHUTDOWN_GRACE = 500;
// difference in shutdown between RELAY_DAC and RELAY_PI
//byte SHUTDOWN_DIFF = 100;


PowerManager::PowerManager() {
    rpi_state = STARTUP;
    //powerled = powerled();

    Serial.begin(9600);

    pinMode(RELAY_DAC, OUTPUT);
    pinMode(RELAY_RPI, OUTPUT);
    pinMode(RPI_BOOTED, INPUT);
    pinMode(RPI_OFF, INPUT);
    pinMode(PWRBUTTON, INPUT_PULLUP);
    pinMode(RPI_SHUTDOWN, OUTPUT);
    digitalWrite(RPI_SHUTDOWN, LOW);
//    power_on_pi();
    pi_shutdown_detected = 0;
    Serial.println("pwrmgr init");
}

void PowerManager::power_off_pi() {
    digitalWrite(RELAY_RPI, HIGH);
    delay(SHUTDOWN_DIFF);
    digitalWrite(RELAY_DAC, HIGH);
    rpi_state = POWER_OFF;
    power = false;
}

void PowerManager::power_on_pi() {
    digitalWrite(RELAY_DAC, LOW);
    delay(SHUTDOWN_DIFF);
    digitalWrite(RELAY_RPI, LOW);
    rpi_state = STARTUP;
    power = true;
}

void PowerManager::listen_pwr_button() {
    // rpi is currently up
    int pwrbutton_status = digitalRead(PWRBUTTON);
    if (pwrbutton_status == HIGH) {
        Serial.println("PWR BUTTON pressed");
        if (power == true){
            digitalWrite(RPI_SHUTDOWN, HIGH);
            rpi_state = SHUTDOWN;
        }
        // rpic is currently down
        else {
            power_on_pi();
//            rpi_state = STARTUP;
        }
    }
}

void PowerManager::check_for_poweroff() {
    // if shutdown pin is high, wait SHUTDOWN_GRACE and power off
    shutoff_pin = digitalRead(RPI_OFF);
    digitalWrite(LED_BUILTIN, shutoff_pin);   // turn the LED on

    // first press
    if (shutoff_pin == HIGH && pi_shutdown_detected == 0) {
        pi_shutdown_detected = millis() + SHUTDOWN_GRACE;
    }
    // SHUTDOWN_GRACE time has passed
    else if (pi_shutdown_detected < millis()) {
        // and button is still pressed
        if (shutoff_pin == HIGH) {
            // turn off
            Serial.println("Shutting off RPi");
            digitalWrite(RPI_SHUTDOWN, LOW);
            rpi_state = POWER_OFF;
            power_off_pi();
        }
        // button not pressed anymore
        else {
            // reset
            pi_shutdown_detected = 0;
        }
    }
}

void PowerManager::loop(){
  listen_pwr_button();
  if (digitalRead(RPI_BOOTED) == HIGH) {
      rpi_state = POWER_ON;
  }

/*
  if (rpi_state == STARTUP):
  else if (rpi_state == POWER_ON):
  else if (rpi_state == SHUTDOWN):
  else if (rpi_state == POWER_OFF:
*/

  powerled.show_state(rpi_state);
  check_for_poweroff();
}
