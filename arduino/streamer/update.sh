#!/usr/bin/env bash
git pull
# pip install platformio is needed
#pio run
if [ $? -ne 0 ]
then
   exit 1
fi
pio run -t upload
if [ $? -ne  0 ]
then
   exit 2
fi
pio device monitor

