#!/usr/bin/env python3
#
# Bitbang'd SPI interface with an MCP3008 ADC device
# MCP3008 is 8-channel 10-bit analog to digital converter
#  Connections are:
#     CLK => 18
#     DOUT => 23 (chip's data out, RPi's MISO)
#     DIN => 24  (chip's data in, RPi's MOSI)
#     CS => 25

import RPi.GPIO as GPIO
import time
import sys


class SPI:
    #def __init__(self, freq, CLK=23, MISO=21, MOSI=19, CS=24):
    def __init__(self, freq, CLK=11, MISO=9, MOSI=10, CS=8):
        ''' init with standard spidev pins '''
        self.CLK = CLK
        self.MISO = MISO
        self.MOSI = MOSI
        self.CS = CS
        self.freq = freq
        self.wait = 1 / float(freq) / 2
        self.GPIO = GPIO

        # RPi.GPIO Layout verwenden (wie Pin-Nummern)
#        self.GPIO.setmode(GPIO.BOARD)
        self.GPIO.setmode(GPIO.BCM)

        # Pin 18 (GPIO 24) auf Input setzen
#        for pin in [self.CLK, self.MOSI, self.CS]:
#            self.GPIO.setup(pin, GPIO.OUT)
        self.GPIO.setup(self.CLK, GPIO.OUT)
        self.GPIO.setup(self.MOSI, GPIO.OUT)
        self.GPIO.setup(self.CS, GPIO.OUT)
        self.GPIO.setup(self.MISO, GPIO.IN)

        # init CS high
        self.GPIO.output(self.CS, GPIO.HIGH)
        self.GPIO.output(self.CLK, GPIO.HIGH)
        self.GPIO.output(self.MOSI, GPIO.HIGH)

    def __del__(self):
        ''' clean up gpio '''
        self.GPIO.cleanup()

    def tick(self):
        self.GPIO.output(self.CLK, GPIO.HIGH)
        time.sleep(self.wait)

    def tock(self):
        self.GPIO.output(self.CLK, GPIO.LOW)
        time.sleep(self.wait)

    def tobits(self,mybytes):
        bits=[]
        for mybyte in mybytes:
            for i in range (7,-1,-1):
                bit = (mybyte&(2**i) != 0)
                bits.append(bit)
        return bits

    def writebytes(self, data):
        bitdata = self.tobits(data)
        # start - set CS down
#        self.GPIO.setmode(GPIO.BOARD)
        self.GPIO.output(self.CS, GPIO.LOW)
        self.tock()

        for bit in bitdata:
            # set data bit
            if bit:
                self.GPIO.output(self.MOSI, GPIO.HIGH)
            else:
                self.GPIO.output(self.MOSI, GPIO.LOW)
            # set pulse high
            self.tick()
            # set pulse low
            self.tock()

        #end - set CS high
        self.tick()
        self.GPIO.output(self.CS, GPIO.HIGH)


if __name__ == '__main__':
    try:
#        GPIO.setmode(GPIO.BOARD)
#        setupSpiPins(CLK, MISO, MOSI, CS)

        while True:
            val = readAdc(0, CLK, MISO, MOSI, CS)
            print("ADC Result: ", str(val))
            time.sleep(5)
    except KeyboardInterrupt:
        GPIO.cleanup()
        sys.exit(0)
