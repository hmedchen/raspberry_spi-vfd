#!/usr/bin/env python3
import spidev
import sys
import time
import spilib
import subprocess
import json

# all credit to the Adafruit SPI VFD arduino library
class SpiVfdModes:
    def __init__(self):
        # commands
        self.CLEARDISPLAY=0x01
        self.RETURNHOME=0x02
        self.ENTRYMODESET=0x04
        self.DISPLAYCONTROL=0x08
        self.CURSORSHIFT=0x10
        self.FUNCTIONSET=0x30
        self.SETCGRAMADDR=0x40
        self.SETDDRAMADDR=0x80
        # flags for display entry mode
        self.ENTRYRIGHT=0x00
        self.ENTRYLEFT=0x02
        self.ENTRYSHIFTINCREMENT=0x01
        self.ENTRYSHIFTDECREMENT=0x00
        # flags for display on/off control
        self.DISPLAYON=0x04
        self.DISPLAYOFF=0x00
        self.CURSORON=0x02
        self.CURSOROFF=0x00
        self.BLINKON=0x01
        self.BLINKOFF=0x00
        # flags for display/cursor shift
        self.DISPLAYMOVE=0x08
        self.CURSORMOVE=0x00
        self.MOVERIGHT=0x04
        self.MOVELEFT=0x00
        # flags for function set
        self.DOUBLE_LINE=0x08
        self.SINGLE_LINE=0x00
        self.BRIGHTNESS25=0x03
        self.BRIGHTNESS50=0x02
        self.BRIGHTNESS75=0x01
        self.BRIGHTNESS100=0x00

        self.SPICOMMAND=0xF8
        self.SPIDATA=0xFA

# When the display powers up, it is configured as follows:
#
#  1. Display clear
#  2. Function set:
#     N = 1; 2-line display
#     BR1=BR0=0; (100% brightness)
#  3. Display on/off control:
#     D = 0; Display off
#     C = 0; Cursor off
#     B = 0; Blinking off
#  4. Entry mode set:
#     I/D = 1; Increment by 1
#     S = 0; No shift
#
# Note, however, that resetting the Arduino doesn't reset the LCD, so we
# can't assume that its in that state when a sketch starts (and the
# SPI_VFD constructor is called).
class VFD:
    def __init__(self, cols=24, lines=2, spi_num = 0, spi_cd = 0):
        # import command constants
        self.cmd = SpiVfdModes()
        # not debug
        self.debug = False
        # store parameters
        self.spi_num = spi_num
        self.spi_cd = spi_cd
        self.cols = cols
        self.lines = lines
        # a few states
        self.x = 0
        self.y = 0
        # a few defaults
        self.display = self.cmd.DISPLAYON
        self.cursor = self.cmd.CURSOROFF
        self.blink = self.cmd.BLINKOFF
        self.brightness = self.cmd.BRIGHTNESS100
        # open spi device - this did not work for me, so I had to write
        # my own SPI class
        self.spi = spilib.SPI(15600000)
#        self.spi = spidev.SpiDev()
#        self.spi.open(spi_num, spi_cd)
#        self.spi.cshigh = True
        #self.spi.max_speed_hz = 15600000

        # mimic'ed the arduino boot up sequence
        self.cls()
        self.home()
        self.func_set()
        self.set_ltor(True)
        self.setpos(0,0)
        self.update_display()
        self.cls()
        self.home()

    def cls(self):
        ''' clear screen '''
        self._command(self.cmd.CLEARDISPLAY, "clscrn")

    def func_set(self):
        ''' set num lines, luminance, bits '''
        self._command(self.cmd.FUNCTIONSET  | self.cmd.DOUBLE_LINE | self.brightness, "bright")

    def set_ltor(self, shift=True):
        ''' set text flow left to right '''
        if shift:
            displaymode = self.cmd.ENTRYLEFT | self.cmd.ENTRYSHIFTDECREMENT
        else:
            displaymode = self.cmd.ENTRYLEFT
        self._command(self.cmd.ENTRYMODESET | displaymode, "dspl2r")

    def set_rtol(self, shift=True):
        ''' set text flow right to left '''
        if shift:
            displaymode = self.cmd.ENTRYRIGHT | self.cmd.ENTRYSHIFTINCREMENT
        else:
            displaymode = self.cmd.ENTRYRIGHT
        self._command(self.cmd.ENTRYMODESET | displaymode, "dspr2l")

    def home(self):
        ''' set screen pos to 0,0 '''
        self._command(self.cmd.RETURNHOME, "gohome")

    def scroll_left(self):
        self._command(self.cmd.CURSORSHIFT | self.cmd.DISPLAYMOVE | self.cmd.MOVELEFT);

    def scroll_right(self):
        self._command(self.cmd.CURSORSHIFT | self.cmd.DISPLAYMOVE | self.cmd.MOVERIGHT);


    def setpos(self, x=0, y=0):
        #TODO: Other than 0/0
        xaddr = x
        yaddr = 0x40 * y
        self._command(self.cmd.SETDDRAMADDR | xaddr | yaddr, "setpos")

    def set_char_pos(self,pos):
        self._command(self.cmd.SETCGRAMADDR | 0x40 | pos, "setcg@")

    def write(self, output):
        ''' write output string to display '''
        for c in output:
            data = [self.cmd.SPIDATA, ord(c)]
            self.spi.writebytes(data[:])
            if self.debug:
                print("write ("+c+")\t->", self._binstr(data))

    def writebytes(self, output):
        ''' write output string to display '''
        data = [self.cmd.SPIDATA, output]
        self.spi.writebytes(data[:])
        if self.debug:
            print("write \t->", bin(output))
#            time.sleep(0.001)

    def set_brightness(self, bright):
        ''' set brightness of display (4=full, 1=least) '''
        if bright == 1:
            self.brightness = self.cmd.BRIGHTNESS25
        elif bright == 2:
            self.brightness = self.cmd.BRIGHTNESS50
        elif bright == 3:
            self.brightness = self.cmd.BRIGHTNESS75
        else:
            self.brightness = self.cmd.BRIGHTNESS100
        self.func_set()

    def set_display(self, state, update=True):
        ''' turn on/off display '''
        if state == True:
            self.display = self.cmd.DISPLAYON
        else:
            self.display = self.cmd.DISPLAYOFF
        if update:
            self.update_display()

    def set_cursor(self, state, update=True):
        ''' set cursor position on/off '''
        if state == True:
            self.cursor = self.cmd.CURSORON
        else:
            self.cursor = self.cmd.CURSOROFF
        if update:
            self.update_display()

    def set_blink(self, state, update=True):
        ''' set cursor blink on/off '''
        if state == True:
            self.blink = self.cmd.BLINKON
        else:
            self.blink = self.cmd.BLINKOFF
        if update:
            self.update_display()

    def update_display(self):
        ''' update display after changed settings '''
        return self._command(self.cmd.DISPLAYCONTROL | self.display | self.cursor | self.blink, "upddsp")

    def createChar(self, location, charmap):
#        location &= 0x7; // we only have 8 locations 0-7
        if location > 7 or location < 0:
            return false
        else:
            self._command(self.cmd.SETCGRAMADDR | (location << 3))
            for i in range(8):
                self.writebytes(charmap[i])

    def _command(self, cmd, cmdstr="UNKNWN"):
        ''' send command to display '''
        data = [self.cmd.SPICOMMAND, cmd]
        r = self.spi.writebytes(data[:])
        r = [0,0]
        if self.debug:
            print("cmd",cmdstr,"\t->", self._binstr(data[:]), "<-", self._binstr(r[:]))
        return r
        time.sleep(0.001)

    def _binstr(self,data):
        ''' convert char to binary, just for aligning debug output '''
        ret=[]
        for i in data:
            ret.append(bin(i)[2:].zfill(8))
        return "["+"|".join(ret)+"]"
