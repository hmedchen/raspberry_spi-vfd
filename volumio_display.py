#!/usr/bin/env python3
import vfd
import sys
import subprocess
import json
import time
import deamon
import requests
import logging
import RPi.GPIO as gpio
from argparse import ArgumentParser
from datetime import datetime


STATUS_REST = 0
STATUS_CLI = 1
STATUS_METHOD = STATUS_REST


class VolumioDisplay(deamon.Deamon):
    def __init__(self, args):
        self.STAT_STOP = 0
        self.STAT_PLAY = 1
        self.STAT_PAUSE = 2
        self.UNKNOWN = 3

        #self.GPIO_BOOTED = 36
        #self.GPIO_SHUTDOWN_INIT = 38
        self.GPIO_BOOTED = 16
        self.GPIO_SHUTDOWN_INIT = 26

        self.vol = 0
        self.vol_prev = 0
        self.voltime = 0
        self.volstr = "   "

        self.volumio_version="unknown"
        self._get_volumio_version()

        self.volumio_IP="unknown"
        self._get_volumio_IP()

        self.play_status = self.UNKNOWN
        self.play_status_prev = self.play_status

        self.titlepos = 0
        self.waits = 0
        # number of cycles the end of the song is displayed
        self.maxwaits = 10

        self.args = args
        if self.args.fg:
            super(VolumioDisplay, self).__init__(deamonize=False, force=args.force)
        else:
            super(VolumioDisplay, self).__init__(force=args.force)
        self.disp = vfd.VFD(24,2,0,1)
        self.vstatus = {}
        hifi=[ 0b00000, 0b10100, 0b11101, 0b10101, 0b00000, 0b11100, 0b11101, 0b10001]
        lofi=[ 0b00000, 0b10000, 0b10001, 0b11101, 0b00000, 0b11100, 0b11101, 0b10001]
        stopchar=[ 0b00000, 0b00000, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b00000 ]
        threedots=[ 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b10101 ]
        self.disp.createChar(1,hifi)
        self.disp.createChar(2,lofi)
        self.disp.createChar(3,stopchar)
        self.disp.createChar(4,threedots)
        self.symbols={"stop": '\x01',
                    "play": '\x10',
                    "pause": '\xA0',
                    "hifi": '\x02',
                    "lofi": '\x03',
                    "threedots": '\x04'}

        self.volfadetime = 1.5
        self.fadetime = 3
        self.clocktime = 5

        gpio.setmode(gpio.BCM)
#        gpio.setmode(gpio.BOARD)
       	gpio.setup(self.GPIO_BOOTED,gpio.OUT)
       	gpio.setup(self.GPIO_SHUTDOWN_INIT,gpio.IN)

        # turning off requests logging
        logging.getLogger("urllib3").setLevel(logging.WARNING)

    def disp_active(self, active):
        if active is False:
            #self.disp.set_display(False)
            self.disp.set_brightness(1)
        else:
            #self.disp.set_display(True)
            self.disp.set_brightness(4)

    def _signal_booted(self):
#        print "booted"
        gpio.output(self.GPIO_BOOTED,True)

    def _get_shutdown_signal(self):
        if gpio.input(self.GPIO_SHUTDOWN_INIT) == 1:
            gpio.output(self.GPIO_BOOTED,False)
            print("Shutdown initiated!")
            subprocess.call(["sudo","shutdown","-h","now"])

    def _catch_signal(self, sig, frame):
        ''' makes sure the server exits gracefully when killed '''
        self.disp.cls()
        self.disp.setpos(0,5)
        self.disp.write("Good-bye!")
        time.sleep(0.2)
        self.disp.set_display(False)
        self.disp_active(False)
        super(VolumioDisplay, self)._catch_signal(sig, frame)

    def _getts(self, keyname):
        return self.vstatus[keyname]

    def _getkey(self, keyname):
        try:
            return self.vstatus[keyname]
        except:
            return "-unknown-"

    def disp_date(self):
        now = datetime.now()
        datestr = now.strftime("%a, %d.%m.%Y")
        self.disp.setpos(0,0)
        self.disp.write(datestr.center(24))
        timestr = now.strftime("%H:%M")
        self.disp.setpos(0,1)
        self.disp.write(timestr.center(24))

    def disp_greeting(self):
        if self.volumio_IP == '':
            self._get_volumio_IP()
        infostr0="Welcome to Volumio"
        infostr1="IP " + self.volumio_IP + " v" +self.volumio_version
        self.disp.setpos(0,0)
        self.disp.write(infostr0.center(24))
        self.disp.setpos(0,1)
        self.disp.write(infostr1.center(24))

    def disp_song(self):
        try:
            status = self.symbols[self._getkey("status")]
        except:
            status = "?"

        #artist_title = (self._getkey("artist") + " - " + self._getkey("title"))[:25].ljust(24)
        title_line = (self._getkey("title") + " - " + self._getkey("artist"))

        # scroll doesn't work here, as it scrolls both lines
        if len(title_line) > 24:
            if self.titlepos >= len(title_line) + 1 - 24:
                if self.waits < self.maxwaits:
                    display_title = title_line[-24:]
                    self.waits += 1
                else:
                    display_title = title_line[:23] + self.symbols['threedots']
            else:
                display_title = title_line[self.titlepos:24+self.titlepos] + " "
                self.titlepos += 1
        else:
            display_title = title_line.ljust(24," ")

        tracktype = self._getkey("trackType")[:8].ljust(8)

        try:
            print (self._getts("seek"), self._getts("duration"))
            seek = self._to_time(self._getts("seek") // 1000) +\
               "/" + self._to_time(self._getts("duration"))
            print (seek)
        except TypeError:
            seek = "n/a"
        self.disp.setpos(0,0)
        self.disp.write(display_title)
        self.disp.setpos(0,1)
        self.disp.write(status.ljust(2," "))
        self.disp.setpos(2,1)
        self.disp.write(tracktype.ljust(8," "))
        self.disp.setpos(10,1)
        self.disp.write(self.volstring.ljust(5," "))
        self.disp.setpos(15,1)
        self.disp.write(seek)
        #print title_line
        #print status, seek
        #for i in range(2):
        #    sys.stdout.write("\033[F")

    def updateVolumeString(self):
        # save old volume
        self.vol_prev = self.vol
        # get new volume
        try:
            self.vol = self.vstatus["volume"]
        except:
            self.vol = 0

        curtime = time.time()
        # if volume didn't change for x seconds, remove display
        if self.vol_prev == self.vol and curtime - self.voltime > self.volfadetime:
            self.volstring = "   "
            return
        else:
            # compile volume change display
            if self.vol_prev < self.vol:
                self.voltime = curtime
                self.volstring = '\x1E'+str(self.vol).rjust(2)
            elif self.vol_prev > self.vol:
                self.voltime = curtime
                self.volstring = '\x1F'+str(self.vol).rjust(2)
            return

    def mainloop(self):
        # volume init
        volume = 0
        volstring = "   "
        voltime = time.time()
        pausetime = time.time()
        stoptime = time.time()
        # display off init
        pause = True
        screensaver = False
        while True:
# while ARduino is broken
#            self._get_shutdown_signal()

            if self._get_volumio_status() is False:
                #print "Volumio not up"
                self.disp_greeting()
                time.sleep(0.2)
                continue
            self._signal_booted()

            # get volume string right, including direction and timeout
            self.updateVolumeString()

            # display song info
#            print "Play status:", self.play_status
            if self.play_status == self.STAT_PLAY:
                self.disp_song()
                # if starting to play, turn display bright
                if self.play_status_prev != self.STAT_PLAY:
                    self.disp.set_brightness(4)
                    self.titlepos = 0
                    self.waits = 0

     	    # turn display brighter if in play
            elif self.play_status == self.STAT_PAUSE:
                self.disp_song()
                # if just paused, start timer
                if self.play_status_prev != self.STAT_PAUSE:
                    pausetime = time.time()
                # if pausetime expired, make display darker
                if time.time() - pausetime > self.volfadetime:
                    self.disp.set_brightness(1)
            else:
                # if just stopped, start timer
                if self.play_status_prev != self.STAT_STOP:
                    stoptime = time.time()
                    # safety net if playlist is force cleared - removes gibberish
                    if self._getkey("artist") == "":
                        stoptime -= 10
                # if stoptime expired, display date,
                if time.time() - stoptime > self.clocktime:
                    self.disp.set_brightness(1)
                    self.disp_date()
                else:
                    self.disp.set_brightness(4)
                    self.disp_song()

            # old status is new status
            self.play_status_prev = self.play_status

            time.sleep(0.2)
            #time.sleep(0.05)


    def _get_volumio_status(self):
        if STATUS_METHOD == STATUS_CLI:
            proc = subprocess.Popen(['volumio', 'status'] , stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            out = proc.communicate()
            try:
                self.vstatus = json.loads(str(out[0]))
            except ValueError:
                self.vstatus = ""
        else:
            try:
                resp = requests.get('http://volumio:3000/api/v1/getState')
                if resp.status_code != 200:
                    # This means something went wrong.
                    self.vstatus = ""
                    #raise ApiError('GET /tasks/ {}'.format(resp.status_code))
                self.vstatus = resp.json()
            except requests.exceptions.ConnectionError:
                self.vstatus = ""

        if len(self.vstatus) == 0:
            return False
        # limiting string parsing by filling var with constant
        try:
            if self.vstatus["status"] == "play":
                self.play_status = self.STAT_PLAY
            elif self.vstatus["status"] == "pause":
                self.play_status = self.STAT_PAUSE
            else:
                self.play_status = self.STAT_STOP
        except:
            print(type(self.vstatus), self.vstatus)
            return False
        return True

    def _get_volumio_version(self):
        proc = subprocess.Popen(['grep', 'VOLUMIO_VERSION', '/etc/os-release'] , stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out = proc.communicate()
        self.volumio_version = str(out[0]).split('=')[1].strip().strip('\"')

    def _get_volumio_IP(self):
        proc = subprocess.Popen(['hostname', '-I'] , stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out = proc.communicate()
        self.volumio_IP = str(out[0]).split()[0].strip()
        if self.volumio_IP == '':
            time.sleep(1)
            proc = subprocess.Popen(['hostname', '-I'] , stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            out = proc.communicate()
            if str(out[0]).strip() == '':
                self.volumio_IP = "not known yet."
            self.volumio_IP = str(out[0]).split()[0].strip()

    def _to_time(self, seconds):
        return str(seconds//60)+":"+str(seconds%60).zfill(2)

def create_arg_parser():
    # parse command line
    parser = ArgumentParser()
    parser.add_argument('-f', '--foreground', action="store_true", dest="fg",
                        help="run in foreground, no deamon mode (debugging)", default=False)
    parser.add_argument('-F', '--Force', action="store_true", dest="force",
                        help="Force start up, kill existing process", default=False)
    return parser.parse_args()

def main(*argv):
    args = create_arg_parser()
    disp = VolumioDisplay(args)
    disp.mainloop()

if __name__ == "__main__":
    main(sys.argv)
