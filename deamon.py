#!/usr/bin/env python
import os
import time
import sys
import fcntl
import signal
import logging
import struct

# verbosity can be very expensive
verbose = True

# keeping the base directory here for easier maintenance
default_port = 8042


class Deamon(object):
    #
    # Deamon base class
    # base functionalities:
    # - logging (error, log)
    # - single instance enforcement
    # - lock file support
    # - catching signal, exit cleanup
    # - different verbosity levels
    #
    def __init__(self, verbose=False, deamonize=True, log=True, logdir='./log/', force=False):
        # init signals
        signal.signal(signal.SIGTERM, self._catch_signal)
        signal.signal(signal.SIGINT, self._catch_signal)
        signal.signal(signal.SIGABRT, self._catch_signal)
        # logging flags
        self.log = {}
        self.log["log"] = log
        self.log["verbosity"] = verbose
        self.log["path"] = os.path.abspath(logdir)
        self.log["file"] = True
        self.log["log_fn"] = "access.log"
        self.log["stdout"] = True
        self.deamonize = deamonize
        self.pid_file=os.path.join(self.log["path"],".lock")
        self.logger = None
        self.pid = None
        self.force = force

        if os.path.isdir(self.log["path"]) is False:
            print("Log dir does not exist, trying to create")
            try:
                os.mkdir(self.log["path"])
            except OSError:
                print("FATAL: cannot create log directory (%s)- exitting"
                      % (self.log["path"]))
                self.exit(2)

        self._init_logging()
        if self.deamonize == True:
            self._deamonize()

    def info(self, message):
        self.logger.info(message)

    def error(self, message):
        self.logger.error(message)

    def debug(self, message):
        self.logger.debug(message)

    def exit(self, exitretval=0, message=None):
        ''' handling the exit statements. sys.exit() should not be used '''
        if message:
            self.info("Exit. (" + message + ")")
        else:
            self.info("Exit.")
        if self.deamonize:
            os.remove(self.pid_file)
        sys.exit(exitretval)

    # ---- internal methods
    def _deamonize(self):
        ''' starts the server as daemon if requested '''
        self.debug("Starting daemon")
        try:
            fork_pid = os.fork()
        except OSError as e:
            self.error("Fork failed. {0} {1}\n".format(e.strerror, e.errno))
            self.exit(1)

        if fork_pid > 0:
            sys.exit(0)
        else:
            self.pid = os.getpid()
            self.debug("Forked process has PID " + str(self.pid))

            if os.path.isfile(self.pid_file):
                if self.force:
                    with open(self.pid_file,"r") as fp:
                        pid = int(fp.read().strip())
                    #print (pid)
                    self.info("--Force given - killing previous process")
                    try:
                        os.kill(pid, signal.SIGTERM)
                        time.sleep(3)
                    except OSError:
                        pass
                    try:
                        os.remove(self.pid_file)
                    except OSError:
                        pass
                    self._prog_lock(self.pid)
                    self.info("Deamon started")
                else:
                    self.debug("PID File" + self.pid_file + "already exists. Aborting")
                    sys.exit(2)
            else:
                self._prog_lock(self.pid)
                self.info("Deamon started")

    def _prog_lock(self, pid):
        '''
        Make sure the program only runs once.
        With this, the program can be started regularly
        '''
        try:
            fp = open(self.pid_file, "w")
            fcntl.flock(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
            fp.write(str(self.pid) + "\n")
            fcntl.flock(fp, fcntl.LOCK_UN)
        except (OSError, IOError):
            if fp:
                fp.close()
            raise

    def _init_logging(self):
        '''
        initialize logging
        '''
        self.logger = logging.getLogger()
        # clearing existing handlers to prevent dupe logging
        for handler in self.logger.handlers:
            self.logger.removeHandler(handler)

        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

        if self.deamonize:
            self.log["stdout"] = False
            self.log["file"] = True

        if self.log["file"]:
            lfh = logging.FileHandler(
                os.path.join(self.log["path"], self.log["log_fn"]))
            lfh.setFormatter(formatter)
            self.logger.addHandler(lfh)
        if self.log["stdout"]:
            lsh = logging.StreamHandler(stream=sys.stdout)
            lsh.setFormatter(formatter)
            self.logger.addHandler(lsh)
        if self.log["verbosity"] == True:
            self.logger.setLevel(logging.DEBUG)
            self.debug("Set logging level to 'debug'")
        else:
            self.logger.setLevel(logging.INFO)

    def _catch_signal(self, sig, frame):
        ''' makes sure the server exits gracefully when killed '''
        if sig == signal.SIGINT:
            self.exit(0, "Received SIGINT - Exitting.")
        elif sig == signal.SIGTERM:
            self.exit(10, "Received SIGTERM - Exitting")
        else:
            self.exit(20, "Unknown Signal - Exitting")


def main(argv):
    d = Deamon()
    time.sleep(5)
    d.exit(0,"Bye")


if __name__ == "__main__":
    main(sys.argv)
